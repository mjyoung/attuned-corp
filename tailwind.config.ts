import { type Config } from 'tailwindcss';

export default {
  content: ['./src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        black: '#1d1d1d',
        white: '#fdfdfd',
      },
    },
  },
  plugins: [],
} satisfies Config;
