# Setup

1. Create a `.env` file in the root directory and add the `DATABASE_URL` (example in `.env.example`)

2. Install dependencies

```bash
pnpm install (or yarn or npm)
```

3. Migrate the database

```bash
pnpm db:migrate
```

4. Run in dev:

```bash
pnpm dev
```

# Stack

- TypeScript
- Next.js
- Prisma ORM
- Postgres
- TailwindCSS

# To do if given more time

- Didnt get to the "User online" requirement
- Use websockets
- One-way hash user passwords
- Better UI in general
- Add microinteractions / animations for new incoming messages
- Add a "typing" indicator
- Refactor auth logic in general (either with JWT or server-side sessions)
