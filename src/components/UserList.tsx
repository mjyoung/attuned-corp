import { type User } from '@prisma/client';
import Link from 'next/link';
import { useState, useEffect, useContext } from 'react';
import AppContext from '~/contexts/AppContext';

export default function UserList() {
  const { user: loggedInUser } = useContext(AppContext);
  const [users, setUsers] = useState<User[]>([]);
  useEffect(() => {
    const fetchUsers = async () => {
      await fetch('/api/users')
        .then((res) => res.json())
        .then((data) => setUsers(data.data));
    };
    fetchUsers();
  }, []);

  if (!users) {
    return null;
  }
  return (
    <section className="max-w-[300px]">
      <ul className="flex flex-col gap-4">
        {users
          .filter((user) => user.id !== loggedInUser?.id)
          .map((user) => {
            const chatSlug = [user.username, loggedInUser?.username]
              .sort()
              .join('-');
            return (
              <Link
                key={user.id}
                className="min-w-[150px] rounded-md bg-white px-4 py-2 text-black hover:bg-gray-200"
                href={`/chats/${chatSlug}`}
              >
                <li>{user.username}</li>
              </Link>
            );
          })}
      </ul>
    </section>
  );
}
