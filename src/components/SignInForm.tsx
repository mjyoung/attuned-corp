import { useRouter } from 'next/router';
import { useState } from 'react';

export default function SignInForm() {
  const router = useRouter();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  return (
    <section>
      <div>
        <label htmlFor="username">Username</label>
        <input
          className="rounded-md px-2 py-1 text-black"
          id="username"
          type="text"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        ></input>
      </div>
      <div>
        <label htmlFor="password">Password</label>
        <input
          className="rounded-md px-2 py-1 text-black"
          id="password"
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        ></input>
      </div>
      <button
        className="rounded-md bg-emerald-600 px-4 py-2"
        onClick={async () => {
          await fetch('/api/signin', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({ username, password }),
          });
          router.push('/');
        }}
      >
        Login
      </button>
    </section>
  );
}
