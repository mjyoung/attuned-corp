export default function Header() {
  return (
    <div className="w-full border-b border-b-gray-600 bg-gray-900 px-4 py-4">
      <div className="container mx-auto flex w-full items-center justify-center gap-5 sm:justify-start">
        <img
          className="h-10"
          src="https://app.attunedcorp.com/attuned-logo-dark-mode.svg"
          alt="Attuned logo"
        />

        <h1 className="text-xl sm:text-3xl">Attuned x Michael Young</h1>
        <img
          className="h-10 rounded-full"
          src="https://media.licdn.com/dms/image/D5603AQGhLDevSXXkIA/profile-displayphoto-shrink_800_800/0/1680202580240?e=1698883200&v=beta&t=ymreyDK3YtTX4y8o5doPBeJQCyRC7HM9NY6yjwDRBag"
          alt="Michael Young image"
        />
      </div>
    </div>
  );
}
