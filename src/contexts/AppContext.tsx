import { type ReactNode, createContext, useState } from 'react';

const AppContext = createContext<{
  user: { id: string; username: string } | null;
  setUser: any;
}>({
  setUser: () => null,
  user: null,
});

interface Props {
  children: ReactNode;
}

export const AppContextProvider = function ({ children }: Props) {
  const [user, setUser] = useState(null);
  return (
    <AppContext.Provider value={{ setUser, user }}>
      {children}
    </AppContext.Provider>
  );
};

export default AppContext;
