import { type NextApiRequest, type NextApiResponse } from 'next';

interface Req extends NextApiRequest {
  body: Record<string, any>;
}

export default function handler(req: Req, res: NextApiResponse) {
  if (req.method === 'GET') {
    return res.status(200).json({ status: 'OK' });
  }
}
