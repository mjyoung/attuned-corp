import { type NextApiRequest, type NextApiResponse } from 'next';
import { prisma } from '~/server/db';

interface Req extends NextApiRequest {
  body: Record<string, any>;
}

export default async function handler(req: Req, res: NextApiResponse) {
  if (req.method === 'GET') {
    const users = await prisma.user.findMany({
      select: {
        id: true,
        username: true,
      },
    });
    return res.status(200).json({ data: users });
  }
}
