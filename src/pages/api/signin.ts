import { type NextApiRequest, type NextApiResponse } from 'next';
import { prisma } from '~/server/db';

interface Req extends NextApiRequest {
  body: { username: string; password: string };
}

export default async function handler(req: Req, res: NextApiResponse) {
  const { username, password } = req.body;
  const user = await prisma.user.findUnique({
    where: {
      username,
      password,
    },
  });

  if (!user) {
    return res
      .status(401)
      .json({ error: 'Cannot log in with those credentials' });
  }

  const userValue = {
    id: user.id,
    username: user.username,
  };

  res.setHeader('Set-Cookie', `user=${JSON.stringify(userValue)}; Path=/;`);
  return res
    .status(200)
    .json({ data: { user: { id: user.id, username: user.username } } });
}
