import { isEmpty } from 'lodash-es';
import { type NextApiRequest, type NextApiResponse } from 'next';
import { prisma } from '~/server/db';

interface Req extends NextApiRequest {
  body: { message: string };
}

export default async function handler(req: Req, res: NextApiResponse) {
  if (req.method === 'POST') {
    const { message } = req.body;
    const user = JSON.parse(req.cookies.user ?? '{}');
    const { slug } = req.query;

    const chat = await prisma.chat.findUnique({
      where: {
        slug: slug as string,
      },
    });

    if (!isEmpty(chat) && !isEmpty(user)) {
      const chatMessage = await prisma.chatMessage.create({
        data: {
          message,
          chatId: chat.id,
          sentByUserId: user.id,
        },
      });
      return res.status(200).json({ data: chatMessage });
    }

    return res.status(400).json({ error: 'Bad request' });
  }
}
