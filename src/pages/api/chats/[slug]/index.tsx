import { type NextApiRequest, type NextApiResponse } from 'next';
import { prisma } from '~/server/db';

interface Req extends NextApiRequest {
  body: Record<string, any>;
}

export default async function handler(req: Req, res: NextApiResponse) {
  if (req.method === 'GET') {
    const { slug } = req.query;

    const chat = await prisma.chat.findUnique({
      where: {
        slug: slug as string,
      },
      include: {
        chatMessages: {
          include: { sentByUser: true },
          orderBy: {
            createdAt: 'asc',
          },
        },
        users: {
          select: {
            id: true,
            username: true,
          },
        },
      },
    });

    if (chat) {
      return res.status(200).json({ data: chat });
    }

    // no existing chat, create one
    const usernames = (slug as string).split('-');

    const newChat = await prisma.chat.create({
      data: {
        slug: slug as string,
        users: {
          connect: usernames.map((username) => ({ username })),
        },
      },
    });

    return res.status(200).json({ data: newChat });
  }
}
