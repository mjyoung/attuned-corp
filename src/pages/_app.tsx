import { type AppType } from 'next/dist/shared/lib/utils';
import { Inter } from 'next/font/google';
import clsx from 'clsx';
import Head from 'next/head';
import { CookiesProvider } from 'react-cookie';

import '~/styles/globals.css';
import { AppContextProvider } from '~/contexts/AppContext';

// If loading a variable font, you don't need to specify the font weight
const inter = Inter({ subsets: ['latin'] });

const MyApp: AppType = ({ Component, pageProps }) => {
  return (
    <>
      <Head>
        <script src="https://js.pusher.com/8.0.1/pusher.min.js"></script>
      </Head>
      <CookiesProvider>
        <AppContextProvider>
          <main className={clsx(inter.className, 'bg-gray-900 text-white')}>
            <Component {...pageProps} />
          </main>
        </AppContextProvider>
      </CookiesProvider>
    </>
  );
};

export default MyApp;
