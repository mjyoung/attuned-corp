import { type Chat, type User, type Prisma } from '@prisma/client';
import clsx from 'clsx';
import { useRouter } from 'next/router';
import { useContext, useEffect, useState } from 'react';
import { useInterval } from 'react-use';
import UserList from '~/components/UserList';
import AppContext from '~/contexts/AppContext';
import MainLayout from '~/layouts/MainLayout';

export default function ChatPage() {
  const router = useRouter();
  const { slug } = router.query;
  const { user: loggedInUser } = useContext(AppContext);

  const [chat, setChat] = useState<Chat | null>(null);
  const [chatMessages, setChatMessages] = useState<
    | Prisma.ChatMessageGetPayload<{
        include: { sentByUser: true };
      }>[]
    | null
  >(null);
  const [users, setUsers] = useState<User[]>([]);
  const [newMessage, setNewMessage] = useState('');

  const fetchChatOnInterval = async () => {
    await fetch(`/api/chats/${slug as string}`)
      .then((res) => res.json())
      .then((data) => {
        setChat(data.data);
        setChatMessages(data.data.chatMessages);
        setUsers(data.data.users);
      });
  };

  useEffect(() => {
    const fetchChatMessages = async () => {
      await fetch(`/api/chats/${slug as string}`)
        .then((res) => res.json())
        .then((data) => {
          setChat(data.data);
          setChatMessages(data.data.chatMessages);
          setUsers(data.data.users);
        });
    };
    if (slug) {
      fetchChatMessages();
    }
  }, [slug]);

  useInterval(() => {
    fetchChatOnInterval();
  }, 2000);

  if (!users) {
    return null;
  }

  return (
    <MainLayout>
      <div className="flex">
        <UserList />
        <section className="ml-6">
          <h2 className="mb-6 text-2xl">
            Chat ({users.map((user) => user.username).join(' and ')})
          </h2>
          <div className="mb-6">
            <ul>
              {chatMessages?.map((message) => (
                <li
                  key={message.id}
                  className={clsx('my-4 rounded-md px-4 py-2', {
                    'bg-blue-600':
                      message.sentByUser.username === loggedInUser?.username,
                    'bg-gray-600':
                      message.sentByUser.username !== loggedInUser?.username,
                  })}
                >
                  {message.message} ({message.sentByUser.username}) (
                  {String(message.createdAt)})
                </li>
              ))}
            </ul>
          </div>
          <div className="flex flex-col gap-2">
            <label>New Message</label>
            <div className="flex gap-4">
              <input
                className="w-full rounded-md px-2 py-1 text-black"
                type="text"
                value={newMessage}
                onChange={(e) => setNewMessage(e.target.value)}
              />
              <button
                className="rounded-md bg-emerald-600 px-4 py-2"
                onClick={() => {
                  fetch(`/api/chats/${slug as string}/messages`, {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({ message: newMessage }),
                  });
                  setNewMessage('');
                }}
              >
                Send
              </button>
            </div>
          </div>
        </section>
      </div>
    </MainLayout>
  );
}
