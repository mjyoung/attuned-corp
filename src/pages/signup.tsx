import SignUpForm from '~/components/SignUpForm';
import MainLayout from '~/layouts/MainLayout';

export default function SignUpPage() {
  return (
    <MainLayout>
      <SignUpForm />
    </MainLayout>
  );
}
