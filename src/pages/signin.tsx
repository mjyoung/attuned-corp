import SignInForm from '~/components/SignInForm';
import MainLayout from '~/layouts/MainLayout';

export default function SignInPage() {
  return (
    <MainLayout>
      <SignInForm />
    </MainLayout>
  );
}
