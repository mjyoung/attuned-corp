import Head from 'next/head';
import { useCookies } from 'react-cookie';
import MainLayout from '~/layouts/MainLayout';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import UserList from '~/components/UserList';

export default function Home() {
  return (
    <>
      <Head>
        <title>Attuned</title>
        <meta name="description" content="Attuned" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <MainLayout>
        <UserList />
      </MainLayout>
    </>
  );
}
