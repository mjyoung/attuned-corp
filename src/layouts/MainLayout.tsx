import Link from 'next/link';
import { useEffect, useContext } from 'react';
import { useCookies } from 'react-cookie';
import { isEmpty } from 'lodash-es';

import Header from '~/components/Header';
import AppContext from '~/contexts/AppContext';
import { useRouter } from 'next/router';

interface Props {
  children: React.ReactNode;
}

export default function MainLayout({ children }: Props) {
  const router = useRouter();
  const [cookies] = useCookies();
  const { user, setUser } = useContext(AppContext);
  useEffect(() => {
    // TODO: Pass cookies in getServerSideProps
    if (!isEmpty(cookies.user)) {
      setUser(cookies.user);
    }
  }, [cookies, setUser]);

  if (!user && router.route !== '/signin' && router.route !== '/signup') {
    return (
      <div className="flex min-h-screen flex-col items-center">
        <Header />
        <div className="w-full px-4">
          <div className="container mx-auto py-6">
            <div className="flex items-center">
              <Link className="text-blue-500 hover:underline" href="/signin">
                Login
              </Link>
              <span className="mx-2">or</span>
              <Link className="text-blue-500 hover:underline" href="/signup">
                Sign Up
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="flex min-h-screen flex-col items-center">
      <Header />
      <div className="w-full px-4">
        <div className="container mx-auto py-6">{children}</div>
      </div>
    </div>
  );
}
